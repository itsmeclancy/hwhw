let handler  = async (m, { conn, usedPrefix }) => {
  conn.reply(m.chat, `
VPS : https://youtu.be/HzZG2GpavGs
VCC : https://youtu.be/bY0nW4deFUA
BOT : ?
`.trim(), m)
}
handler.help = ['tutorial']
handler.tags = ['about']
handler.command = /^(tutorial)$/i
handler.owner = false
handler.mods = false
handler.premium = false
handler.group = false
handler.private = false

handler.admin = false
handler.botAdmin = false

handler.fail = null

module.exports = handler

